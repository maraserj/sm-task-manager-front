export default {
    data: () => ({
        authenticatedUser: null,
    }),
    created () {
        if (this.$store) {
            this.authenticatedUser = this.$store.state.user;
        }
    },
    methods: {
        blockElement: function (element) {
            $(element).block({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    timeout: 2000,
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        unBlockElement: function (element, microtime) {
            setTimeout(function () {
                $(element).unblock();
            }, microtime > 500 ? microtime : 500);
        },
        /**
         * @param message
         * @param title
         */
        successNotify (message, title) {
            new PNotify({
                title: title !== undefined ? title : 'Успешно.',
                text:  message,
                type: 'success'
            });
        },
        errorNotify (message, title) {
            new PNotify({
                title: title !== undefined ? title : 'Произошла ошибка.',
                text:  message,
                type: 'error'
            });
        },
        infoNotify (message, title) {
            new PNotify({
                title: title !== undefined ? title : 'Обратите внимание.',
                text:  message,
                type: 'info'
            });
        },
    },
}
