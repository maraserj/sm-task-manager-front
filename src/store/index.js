import Vue from 'vue'
import Vuex from 'vuex'
import {mutations}  from './mutations'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const state = {
    authenticated: false,
    user: {},
    profile: {},
    activities: [],
    directions: [],
    events: [],
    hasEvents: false
};


export default new Vuex.Store({
    state,
    mutations,
    strict: debug,
    plugins: [createPersistedState()]
})

