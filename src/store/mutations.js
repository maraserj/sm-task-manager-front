export const mutations = {
    // login/logout
    loginUser( state, { user } ) {
        state.user = user;
        state.authenticated = true;
    },

    logoutUser( state ) {
        state.user = null;
        state.activities = null;
        state.directions = [];
        state.projects = [];
        state.authenticated = false;
        state.worktime = null;
        state.events = [];
        state.hasEvents = false;
    },
    unAuth ( state ) {
        state.authenticated = false;
        state.hasEvents = false;
    },
    userUpdate( state,  user  ) {
        if(user != undefined){
            state.user = user;
        }
    },

    // Activity
    activities ( state, activities ) {
        state.activities = activities;
    },

    // Directions
    directions (state , directions ) {
        state.directions = directions;
    },

    //Projects
    projects (state, projects) {
        state.projects = projects;
    },

    //Profile
    profile (state, profile) {
        state.profile = profile;
    },
    avatar(state, avatar) {
        state.user.avatar = avatar;
    },

    //Calendar
    calendarTypes (state, types) {
        state.calendar_types = types;
    },


    //Worktime
    saveWorktime (state, worktime) {
        state.worktime = worktime;
    },

    //Events
    events (state, events) {
        state.events = events;
    },

    hasEvents (state, hasEvents) {
        state.hasEvents = hasEvents;
    }
}

