import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import Main from '../views/Main.vue'
import MainPage from '../views/MainPage.vue'
import Login from '../views/user/Login.vue'
import Signup from '../views/user/Signup.vue'
import PasswordRecovery from '../views/user/PasswordRecovery.vue'
import NewPassword from '../views/user/NewPassword.vue'
import Profile from '../views/user/Profile.vue'
import Account from '../views/user/Account.vue'

import Activity from '../views/Activity.vue'
import Directions from '../views/Directions.vue'
import Projects from '../views/projects/Projects.vue'
import ProjectsCreateEdit from '../views/projects/ProjectsCreateEdit.vue'
import Tasks from '../views/tasks/Tasks.vue'
import TasksView from '../views/tasks/TasksView.vue'
import TasksCreate from '../views/tasks/TasksCreate.vue'
import ProjectsView from '../views/projects/ProjectsView.vue'
import SubProjects from '../views/projects/SubProjects.vue'
import SubProjectsCreateEdit from '../views/projects/SubProjectsCreateEdit.vue'
import Diagrams from '../views/Diagrams.vue'
import Ranks from '../views/Ranks.vue'
import AllNotifications from '../views/AllNotifications.vue'

import NotFound from '../views/errors/404.vue'

const routes = [
    { path: '/', component: MainPage },
    { name: 'index', path: '/index', component: MainPage },
    { name: 'calendar', path: '/main', component: Main },

    { name: 'user.login', path: '/user/login', component: Login },
    { name: 'user.signup', path: '/user/signup', component: Signup },
    { name: 'user.password_reset', path: '/user/password-recovery', component: PasswordRecovery },
    { name: 'user.password_new', path: '/user/new-password', component: NewPassword },
    { name: 'user.profile', path: '/user/profile', component: Profile },
    { name: 'user.account', path: '/user/account', component: Account },

    { name: 'activities.index', path: '/activities', component: Activity },
    { name: 'directions.index', path: '/directions', component: Directions },
    { name: 'projects.index', path: '/projects', component: Projects },
    { name: 'projects.create', path: '/projects/create', component: ProjectsCreateEdit },
    { name: 'projects.edit', path: '/projects/:id/edit', component: ProjectsCreateEdit },
    { name: 'projects.show', path: '/projects/:id/show', component: ProjectsView },
    { name: 'projects.create_sub_project', path: '/projects/:id/sub-project/create', component: SubProjectsCreateEdit },
    { name: 'projects.edit_sub_project', path: '/projects/:id/sub-project/:sub_project_id/edit', component: SubProjectsCreateEdit },
    { name: 'projects.show_sub_project', path: '/projects/:id/sub-project/:sub_project_id/show', component: SubProjects },
    { name: 'tasks.index', path: '/tasks', component: TasksView },
    { name: 'tasks.create', path: '/tasks/create', component: TasksCreate },
    { name: 'tasks.show', path: '/tasks/:id', component: Tasks },
    { name: 'tasks.edit', path: '/tasks/:id/edit', component: TasksCreate },
    { name: 'diagrams', path: '/diagrams', component: Diagrams },
    { name: 'ranks', path: '/ranks', component: Ranks },
    { name: 'notifications', path: '/notifications', component: AllNotifications },

    { path:'*', component: NotFound }
];

export default new VueRouter({
        mode: 'history',
        routes: routes,
        linkActiveClass: 'active'
    });

