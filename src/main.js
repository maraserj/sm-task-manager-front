import Vue from 'vue'
import router from './router';
import VueResource from 'vue-resource'
import VueProgressBar from 'vue-progressbar'
import App from './App.vue'
import store from './store/index.js'
import VeeValidate, { Validator } from 'vee-validate'
import ru from 'vee-validate/dist/locale/ru'
import mixins from './mixins/index'
// require('bootstrap');
// require('bootstrap-sass');


import { sync } from 'vuex-router-sync'



Vue.use(VueResource);

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});
Vue.use(VeeValidate, {locale: 'ru'});

Vue.use(VueProgressBar, {
    color: '#26a69a',
    failedColor: 'red',
    thickness: '3px'
});
Vue.mixin(mixins);

Vue.http.options.xhr = {withCredentials: true};

let showedNotify = false;
Vue.http.interceptors.push(function (request, next){
    let token = localStorage.getItem('token');
        if(token !== '' && request.url !==  API_BASE_URL + '/oauth/token'){
            request.headers.set('Authorization', 'Bearer ' + token);
        }
        next(function(response) {
            if(response.status === 401) {
                localStorage.removeItem('user');
                localStorage.removeItem('token');
                localStorage.setItem('authenticated', false);
                this.$store.commit('unAuth');
                if(!showedNotify){
                    this.errorNotify('Скорее всего, Ваш ключ входа устарел. Пожалуйста, войдите в систему снова.');
                    showedNotify = true;
                }
                router.push('/user/login');
            }
        });
    }
);

router.beforeEach((to, from, next) => {
    let isAuth = store.state.authenticated;

    if (isAuth === false && to.path === '/main') {
        router.push('/user/login');
        next();
    }

    if (isAuth !== true && to.path !== '/user/login') {
        router.push('/user/login');
        next();
    }

    next();
});

sync(store, router);

new Vue({
    el: '#app',
    router: router,
    store: store,
    template: '<App/>',
    components: { App }
});